<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client;

use GuzzleHttp;
use GuzzleHttp\Exception\ClientException as GuzzleClientException;
use OwlLabs\OwlMailman\Client\Exception\ClientException;
use OwlLabs\OwlMailman\Client\Exception\UnexpectedStatusException;
use Zend\Json;

/**
 * Class MailmanApi
 * @package OwlLabs\OwlMailman\Client
 */
class MailmanApi
{
    const VERSION_NUMBER = 1;

    /**
     * @var Configuration
     */
    private $config;

    /**
     * @var GuzzleHttp\Client
     */
    private $client;

    /**
     * GuzzleHttp\Client constructor.
     * @param Configuration $config
     */
    public function __construct(Configuration $config)
    {
        $this->config = $config;
    }

    /**
     * @return Configuration
     */
    public function config(): Configuration
    {
        return $this->config;
    }

    /**
     * @return int
     */
    public function version(): int
    {
        return self::VERSION_NUMBER;
    }

    /**
     * @return Repository\Root
     */
    public function root(): Repository\Root
    {
        return new Repository\Root($this);
    }

    /**
     * @return Repository\Templates
     */
    public function templates(): Repository\Templates
    {
        return new Repository\Templates($this);
    }

    /**
     * @param string $templateId
     * @return Repository\Template
     */
    public function template(string $templateId): Repository\Template
    {
        return new Repository\Template($templateId, $this);
    }

    /**
     * @param string $accountId
     * @return Repository\Account
     */
    public function account(string $accountId): Repository\Account
    {
        return new Repository\Account($accountId, $this);
    }

    /**
     * @param string $path
     * @param array|null $query
     * @return array
     * @throws ClientException
     */
    public function get(string $path, array $query = null): array
    {
        try {
            $response = $this->client()->get($path, [
                'query' => $query,
            ]);
            $contents = $response->getBody()->getContents();
            return Json\Json::decode($contents, Json\Json::TYPE_ARRAY);
        } catch (GuzzleClientException $e) {
            throw ClientException::fromGuzzleException($e);
        }
    }

    /**
     * @param string $path
     * @param array $options
     * @return Repository\Response\Accepted
     * @throws ClientException
     */
    public function post(string $path, array $data): Repository\Response\Accepted
    {
        try {
            $response = $this->client()->post($path, [
                'json' => $data,
            ]);
            if ($response->getStatusCode() !== 202) {
                throw new UnexpectedStatusException('Status was not 202', $response->getStatusCode());
            }
            return Repository\Response\Accepted::fromResponse($response);
        } catch (GuzzleClientException $e) {
            throw ClientException::fromGuzzleException($e);
        }
    }

    /**
     * @param string $path
     * @param array $data
     * @return Repository\Response\Accepted
     * @throws ClientException
     */
    public function patch(string $path, array $data): Repository\Response\Accepted
    {
        try {
            $response = $this->client()->patch($path, [
                'json' => $data,
            ]);
            if ($response->getStatusCode() !== 202) {
                throw new UnexpectedStatusException('Status was not 202', $response->getStatusCode());
            }
            return Repository\Response\Accepted::fromResponse($response);
        } catch (GuzzleClientException $e) {
            throw ClientException::fromGuzzleException($e);
        }
    }

    /**
     * @return GuzzleHttp\Client
     */
    private function client(): GuzzleHttp\Client
    {
        if ($this->client === null) {
            $this->client = new GuzzleHttp\Client([
                'base_uri' => $this->config->uriPrefix(),
                'auth' => [
                    $this->config->username(),
                    $this->config->password(),
                ],
            ]);
        }
        return $this->client;
    }
}
