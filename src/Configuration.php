<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client;

/**
 * Class Configuration
 * @package OwlLabs\OwlMailman\Client
 */
class Configuration
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $uriPrefix;

    /**
     * Configuration constructor.
     * @param string $username
     * @param string $password
     * @param string $uriPrefix
     */
    public function __construct(string $username, string $password, string $uriPrefix)
    {
        $this->username = $username;
        $this->password = $password;
        $this->uriPrefix = $uriPrefix;
    }

    /**
     * @return string
     */
    public function username(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function password(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function uriPrefix(): string
    {
        return $this->uriPrefix;
    }
}
