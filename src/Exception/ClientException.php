<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Exception;

use Zend\Json\Json;

/**
 * Class ClientException
 * @package OwlLabs\OwlMailman\Client\Exception
 */
class ClientException extends \RuntimeException
{
    /**
     * @var string
     */
    private $error;

    /**
     * @var string[][]
     */
    private $errors;

    /**
     * @param \GuzzleHttp\Exception\ClientException $exception
     * @return ClientException
     */
    public static function fromGuzzleException(\GuzzleHttp\Exception\ClientException $exception): ClientException
    {
        $response = $exception->getResponse();

        $statusCode = $response->getStatusCode();
        $data = Json::decode($response->getBody()->getContents(), Json::TYPE_ARRAY);

        $clientException = new self($exception->getMessage(), $statusCode, $exception);

        if (array_key_exists('errors', $data)) {
            $clientException->errors = $data['errors'];
        }

        if (array_key_exists('error', $data)) {
            $clientException->error = $data['error'];
        }

        return $clientException;
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return is_array($this->errors);
    }

    /**
     * @return string[][]
     */
    public function allErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasErrorsFor(string $name): bool
    {
        return is_array($this->errors) && array_key_exists($name, $this->errors);
    }

    /**
     * @param string $name
     * @return string[]
     */
    public function errors(string $name): array
    {
        return $this->errors[$name];
    }

    /**
     * @return bool
     */
    public function hasError(): bool
    {
        return is_string($this->error);
    }

    /**
     * @return string
     */
    public function error(): string
    {
        return $this->error;
    }
}
