<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Exception;

/**
 * Class UnexpectedStatusException
 * @package OwlLabs\OwlMailman\Client\Exception
 */
class UnexpectedStatusException extends \RuntimeException
{
}
