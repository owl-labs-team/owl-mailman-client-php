<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Data\Object;

/**
 * Class TemplateVersionView
 * @package OwlLabs\OwlMailman\Client\Data\Object
 */
class TemplateVersionView
{
    /**
     * @var string
     */
    private $templateId;

    /**
     * @var string
     */
    private $versionId;

    /**
     * @var string
     */
    private $contents;

    /**
     * TemplateVersionView constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->templateId = $data['templateId'];
        $this->versionId = $data['versionId'];
        $this->contents = $data['contents'];
    }

    /**
     * @return string
     */
    public function templateId(): string
    {
        return $this->templateId;
    }

    /**
     * @return string
     */
    public function versionId(): string
    {
        return $this->versionId;
    }

    /**
     * @return string
     */
    public function contents(): string
    {
        return $this->contents;
    }
}
