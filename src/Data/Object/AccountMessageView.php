<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Data\Object;

/**
 * Class AccountMessageView
 * @package OwlLabs\OwlMailman\Client\Data\Object
 */
class AccountMessageView
{
    /**
     * @var string
     */
    private $messageId;

    /**
     * @var string
     */
    private $accountId;

    /**
     * @var string
     */
    private $templateId;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var int
     */
    private $status;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @var string
     */
    private $messageSystemId;

    /**
     * @var \DateTimeImmutable
     */
    private $sentAt;

    /**
     * @todo
     */
    private $addresses;

    /**
     * @param array $data
     * @return \DateTimeImmutable
     */
    private static function createDate(array $data): \DateTimeImmutable
    {
        return new \DateTimeImmutable($data['date'], new \DateTimeZone($data['timezone']));
    }

    /**
     * AccountMessageView constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->messageId = $data['messageId'];
        $this->accountId = $data['accountId'];
        $this->templateId = $data['templateId'];
        $this->subject = $data['subject'];
        $this->status = (int)$data['status'];
        $this->createdAt = self::createDate($data['createdAt']);
        $this->messageSystemId = $data['messageSystemId'];
        $this->sentAt = $data['sentAt'] ? self::createDate($data['sentAt']) : null;
        $this->addresses = $data['addresses'];
    }

    /**
     * @return string
     */
    public function messageId(): string
    {
        return $this->messageId;
    }

    /**
     * @return string
     */
    public function accountId(): string
    {
        return $this->accountId;
    }

    /**
     * @return string
     */
    public function templateId(): string
    {
        return $this->templateId;
    }

    /**
     * @return string
     */
    public function subject(): string
    {
        return $this->subject;
    }

    /**
     * @return int
     */
    public function status(): int
    {
        return $this->status;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function createdAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function messageSystemId(): string
    {
        return $this->messageSystemId;
    }

    /**
     * @return bool
     */
    public function isSent(): bool
    {
        return $this->sentAt instanceof \DateTimeImmutable;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function sentAt(): \DateTimeImmutable
    {
        return $this->sentAt;
    }
}
