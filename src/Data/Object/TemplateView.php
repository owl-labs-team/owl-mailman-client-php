<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Data\Object;

/**
 * Class TemplateView
 * @package OwlLabs\OwlMailman\Client\Data\Object
 */
class TemplateView
{
    /**
     * @var string|null
     */
    private $templateId;

    /**
     * @var string|null
     */
    private $name;

    /**
     * TemplateView constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->templateId = $data['id'];
        $this->name = $data['name'];
    }

    /**
     * @return null|string
     */
    public function templateId(): ?string
    {
        return $this->templateId;
    }

    /**
     * @return null|string
     */
    public function name(): ?string
    {
        return $this->name;
    }
}
