<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Data\Object;

/**
 * Class RootView
 * @package OwlLabs\OwlMailman\Client\Data\Object
 */
class RootView
{
    /**
     * @var string|null
     */
    private $userId;

    /**
     * @var int|null
     */
    private $timestamp;

    /**
     * RootView constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        if (array_key_exists('userId', $data)) {
            $this->userId = $data['userId'];
        }
        if (array_key_exists('timestamp', $data)) {
            $this->timestamp = $data['timestamp'];
        }
    }

    /**
     * @return null|string
     */
    public function userId(): ?string
    {
        return $this->userId;
    }

    /**
     * @return int|null
     */
    public function timestamp(): ?int
    {
        return $this->timestamp;
    }
}
