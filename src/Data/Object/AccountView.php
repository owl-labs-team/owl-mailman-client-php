<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Data\Object;

/**
 * Class AccountView
 * @package OwlLabs\OwlMailman\Client\Data\Object
 */
class AccountView
{
    /**
     * @var string
     */
    private $accountId;

    /**
     * @var string
     */
    private $name;

    /**
     * AccountView constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->accountId = $data['id'];
        $this->name = $data['name'];
    }

    /**
     * @return string
     */
    public function accountId(): string
    {
        return $this->accountId;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}
