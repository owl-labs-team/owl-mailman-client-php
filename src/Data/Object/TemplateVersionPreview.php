<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Data\Object;

/**
 * Class TemplateVersionPreview
 * @package OwlLabs\OwlMailman\Client\Data\Object
 */
class TemplateVersionPreview
{
    /**
     * @var string
     */
    private $html;

    /**
     * TemplateVersionPreview constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->html = $data['html'];
    }

    /**
     * @return string
     */
    public function html(): string
    {
        return $this->html;
    }
}
