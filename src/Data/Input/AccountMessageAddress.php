<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Data\Input;

/**
 * Class AccountMessageAddress
 * @package OwlLabs\OwlMailman\Client\Data\Input
 */
class AccountMessageAddress
{
    /**
     * @var string
     */
    private $emailAddress;

    /**
     * @var null|string
     */
    private $name;

    /**
     * AccountMessageAddress constructor.
     * @param string $emailAddress
     * @param string|null $name
     */
    public function __construct(string $emailAddress, string $name = null)
    {
        $this->emailAddress = $emailAddress;
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'address' => $this->emailAddress,
            'name' => $this->name,
        ];
    }
}
