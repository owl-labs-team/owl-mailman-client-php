<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Data\Input;

/**
 * Class TemplateCreate
 * @package OwlLabs\OwlMailman\Client\Data\Input
 */
class TemplateCreate
{
    /**
     * @var string
     */
    private $name;

    /**
     * TemplateCreate constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'name' => $this->name,
        ];
    }
}
