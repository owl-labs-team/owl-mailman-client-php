<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Data\Input;

/**
 * Class AccountMessageCreate
 * @package OwlLabs\OwlMailman\Client\Data\Input
 */
class AccountMessageCreate
{
    /**
     * @var string
     */
    private $templateId;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var array
     */
    private $variables;

    /**
     * @var AccountMessageAddresses
     */
    private $addresses;

    /**
     * AccountMessageCreate constructor.
     * @param string $templateId
     * @param string $subject
     * @param array $variables
     * @param AccountMessageAddresses $addresses
     */
    public function __construct(
        string $templateId,
        string $subject,
        array $variables,
        AccountMessageAddresses $addresses
    ) {
        $this->templateId = $templateId;
        $this->subject = $subject;
        $this->variables = $variables;
        $this->addresses = $addresses;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'templateId' => $this->templateId,
            'subject' => $this->subject,
            'vars' => $this->variables,
            'addresses' => $this->addresses->toArray(),
        ];
    }
}
