<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Data\Input;

/**
 * Class TemplateVersionModify
 * @package OwlLabs\OwlMailman\Client\Data\Input
 */
class TemplateVersionModify
{
    /**
     * @var string
     */
    private $contents;

    /**
     * TemplateVersionModify constructor.
     * @param string $contents
     */
    public function __construct(string $contents)
    {
        $this->contents = $contents;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'contents' => $this->contents,
        ];
    }
}
