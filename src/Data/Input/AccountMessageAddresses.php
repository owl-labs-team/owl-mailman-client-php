<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Data\Input;

/**
 * Class AccountMessageAddresses
 * @package OwlLabs\OwlMailman\Client\Data\Input
 */
class AccountMessageAddresses
{
    /**
     * @var array
     */
    private $to;

    /**
     * @var array
     */
    private $cc;

    /**
     * @var array
     */
    private $bcc;

    /**
     * @var array
     */
    private $replyTo;

    /**
     * @var array
     */
    private $from;

    /**
     * @param array $addresses
     * @return AccountMessageAddresses
     */
    public static function fromArray(array $addresses): AccountMessageAddresses
    {
        $converted = [
            'to' => [],
            'cc' => [],
            'bcc' => [],
            'replyTo' => [],
            'from' => [],
        ];
        foreach ($addresses as $relation => $addressList) {
            if (!array_key_exists($relation, $converted)) {
                continue;
            }
            foreach ($addressList as $email => $nameOrEmail) {
                if (is_integer($email)) {
                    $converted[$relation][] = new AccountMessageAddress($nameOrEmail);
                } else {
                    $converted[$relation][] = new AccountMessageAddress($email, $nameOrEmail);
                }
            }
        }
        return new self(
            $converted['to'],
            $converted['cc'],
            $converted['bcc'],
            $converted['replyTo'],
            $converted['from']
        );
    }

    /**
     * AccountMessageAddresses constructor.
     * @param AccountMessageAddress[] $to
     * @param AccountMessageAddress[] $cc
     * @param AccountMessageAddress[] $bcc
     * @param AccountMessageAddress[] $replyTo
     * @param AccountMessageAddress[] $from
     */
    public function __construct(array $to, array $cc, array $bcc, array $replyTo, array $from)
    {
        $this->to = $to;
        $this->cc = $cc;
        $this->bcc = $bcc;
        $this->replyTo = $replyTo;
        $this->from = $from;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'to' => $this->addressesToArray($this->to),
            'cc' => $this->addressesToArray($this->cc),
            'bcc' => $this->addressesToArray($this->bcc),
            // 'replyTo' => $this->addressesToArray($this->replyTo), todo unskip when API is ready to take replyTo addresses
            'from' => $this->addressesToArray($this->from),
        ];
    }

    /**
     * @param AccountMessageAddress[] $addresses
     * @return array[]
     */
    private function addressesToArray(array $addresses): array
    {
        return array_map(function (AccountMessageAddress $address) {
            return $address->toArray();
        }, $addresses);
    }
}
