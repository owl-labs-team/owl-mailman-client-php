<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Data\Input;

/**
 * Class TemplateModify
 * @package OwlLabs\OwlMailman\Client\Data\Input
 */
class TemplateModify
{
    /**
     * @var string
     */
    private $name;

    /**
     * TemplateModify constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'name' => $this->name,
        ];
    }
}
