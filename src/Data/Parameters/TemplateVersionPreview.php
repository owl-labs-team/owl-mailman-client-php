<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Data\Parameters;

/**
 * Class TemplateVersionPreview
 * @package OwlLabs\OwlMailman\Client\Data\Parameters
 */
class TemplateVersionPreview
{
    /**
     * @var array
     */
    private $variables;

    /**
     * TemplateVersionPreview constructor.
     * @param array $variables
     */
    public function __construct(array $variables)
    {
        $this->variables = $variables;
    }

    /**
     * @return array
     */
    public function variables(): array
    {
        return $this->variables;
    }
}
