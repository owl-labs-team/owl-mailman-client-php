<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Repository;

use OwlLabs\OwlMailman\Client\Data;
use OwlLabs\OwlMailman\Client\MailmanApi;

/**
 * Class Account
 * @package OwlLabs\OwlMailman\Client\Repository
 */
class Account extends AbstractRepository
{
    /**
     * @var string
     */
    private $accountId;

    /**
     * Account constructor.
     * @param string $accountId
     * @param MailmanApi $api
     */
    public function __construct(string $accountId, MailmanApi $api)
    {
        $this->accountId = $accountId;
        $this->api = $api;
    }

    /**
     * @return string
     */
    public function accountId(): string
    {
        return $this->accountId;
    }

    /**
     * @return AccountMessages
     */
    public function messages(): AccountMessages
    {
        return new AccountMessages($this, $this->api);
    }

    /**
     * @param string $messageId
     * @return AccountMessage
     */
    public function message(string $messageId): AccountMessage
    {
        return new AccountMessage($messageId, $this, $this->api);
    }

    /**
     * @return Data\Object\AccountView
     */
    public function read(): Data\Object\AccountView
    {
        $path = sprintf('/v%d/accounts/%s', $this->api->version(), $this->accountId);
        $data = $this->api->get($path);
        return new Data\Object\AccountView($data);
    }
}
