<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Repository;

use OwlLabs\OwlMailman\Client\MailmanApi;
use OwlLabs\OwlMailman\Client\Data;
use OwlLabs\OwlMailman\Client\Repository\Response\Accepted;

/**
 * Class TemplateVersion
 * @package OwlLabs\OwlMailman\Client\Repository
 */
class TemplateVersion
{
    /**
     * @var string
     */
    private $versionId;

    /**
     * @var Template
     */
    private $templateRepository;

    /**
     * @var MailmanApi
     */
    private $api;

    /**
     * TemplateVersion constructor.
     * @param string $versionId
     * @param Template $templateRepository
     * @param MailmanApi $api
     */
    public function __construct(string $versionId, Template $templateRepository, MailmanApi $api)
    {
        $this->versionId = $versionId;
        $this->templateRepository = $templateRepository;
        $this->api = $api;
    }

    /**
     * @return Data\Object\TemplateVersionView
     */
    public function read(): Data\Object\TemplateVersionView
    {
        $path = sprintf(
            '/v%d/templates/%s/versions/%s',
            $this->api->version(),
            $this->templateRepository->templateId(),
            $this->versionId
        );
        $data = $this->api->get($path);
        return new Data\Object\TemplateVersionView($data);
    }

    /**
     * @param Data\Input\TemplateVersionModify $input
     * @return Accepted
     */
    public function modify(Data\Input\TemplateVersionModify $input): Accepted
    {
        $path = sprintf(
            '/v%d/templates/%s/versions/%s',
            $this->api->version(),
            $this->templateRepository->templateId(),
            $this->versionId
        );
        return $this->api->patch($path, $input->toArray());
    }

    /**
     * @param Data\Parameters\TemplateVersionPreview $parameters
     * @return Data\Object\TemplateVersionPreview
     */
    public function preview(Data\Parameters\TemplateVersionPreview $parameters): Data\Object\TemplateVersionPreview
    {
        $path = sprintf(
            '/v%d/templates/%s/versions/%s/preview',
            $this->api->version(),
            $this->templateRepository->templateId(),
            $this->versionId
        );
        $data = $this->api->get($path, [
            'vars' => json_encode($parameters->variables()),
        ]);
        return new Data\Object\TemplateVersionPreview($data);
    }
}
