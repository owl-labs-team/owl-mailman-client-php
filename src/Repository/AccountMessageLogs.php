<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Repository;

use OwlLabs\OwlMailman\Client\MailmanApi;

/**
 * Class AccountMessageLogs
 * @package OwlLabs\OwlMailman\Client\Repository
 */
class AccountMessageLogs
{
    /**
     * @var AccountMessage
     */
    private $accountMessageRepository;

    /**
     * @var MailmanApi
     */
    private $api;

    /**
     * AccountMessageLogs constructor.
     * @param AccountMessage $accountMessageRepository
     * @param MailmanApi $api
     */
    public function __construct(AccountMessage $accountMessageRepository, MailmanApi $api)
    {
        $this->accountMessageRepository = $accountMessageRepository;
        $this->api = $api;
    }

    public function index()
    {
        // todo
    }
}
