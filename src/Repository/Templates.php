<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Repository;

use OwlLabs\OwlMailman\Client\MailmanApi;
use OwlLabs\OwlMailman\Client\Data\Input;

/**
 * Class Templates
 * @package OwlLabs\OwlMailman\Client\Repository
 */
class Templates extends AbstractRepository
{
    /**
     * Templates constructor.
     * @param MailmanApi $api
     */
    public function __construct(MailmanApi $api)
    {
        $this->api = $api;
    }

    /**
     * @param Input\TemplateCreate $input
     * @return string
     */
    public function create(Input\TemplateCreate $input): string
    {
        $path = sprintf('/v%d/templates', $this->api->version());
        $accepted = $this->api->post($path, $input->toArray());
        return $accepted->getHeaderValue('X-Template-Id');
    }
}
