<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Repository;

use OwlLabs\OwlMailman\Client\Data;
use OwlLabs\OwlMailman\Client\MailmanApi;

/**
 * Class AccountMessage
 * @package OwlLabs\OwlMailman\Client\Repository
 */
class AccountMessage
{
    /**
     * @var string
     */
    private $messageId;

    /**
     * @var Account
     */
    private $accountRepository;

    /**
     * @var MailmanApi
     */
    private $api;

    /**
     * AccountMessage constructor.
     * @param string $messageId
     * @param Account $accountRepository
     * @param MailmanApi $api
     */
    public function __construct(string $messageId, Account $accountRepository, MailmanApi $api)
    {
        $this->messageId = $messageId;
        $this->accountRepository = $accountRepository;
        $this->api = $api;
    }

    /**
     * @return Data\Object\AccountMessageView
     */
    public function read(): Data\Object\AccountMessageView
    {
        $path = vsprintf('/v%d/accounts/%s/messages/%s', [
            $this->api->version(),
            $this->accountRepository->accountId(),
            $this->messageId,
        ]);
        $data = $this->api->get($path);
        return new Data\Object\AccountMessageView($data);
    }
}
