<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Repository;

use OwlLabs\OwlMailman\Client\Data\Input\AccountMessageCreate;
use OwlLabs\OwlMailman\Client\MailmanApi;

/**
 * Class AccountMessages
 * @package OwlLabs\OwlMailman\Client\Repository
 */
class AccountMessages
{
    /**
     * @var Account
     */
    private $accountRepository;

    /**
     * @var MailmanApi
     */
    private $api;

    /**
     * AccountMessages constructor.
     * @param Account $accountRepository
     * @param MailmanApi $api
     */
    public function __construct(Account $accountRepository, MailmanApi $api)
    {
        $this->accountRepository = $accountRepository;
        $this->api = $api;
    }

    /**
     * @param AccountMessageCreate $input
     * @return string
     */
    public function create(AccountMessageCreate $input): string
    {
        $path = sprintf('/v%d/accounts/%s/messages', $this->api->version(), $this->accountRepository->accountId());
        $accepted = $this->api->post($path, $input->toArray());
        return $accepted->getHeaderValue('X-Message-Id');
    }
}
