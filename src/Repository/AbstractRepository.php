<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Repository;

use OwlLabs\OwlMailman\Client\MailmanApi;

/**
 * Class AbstractRepository
 * @package OwlLabs\OwlMailman\Client\Repository
 */
abstract class AbstractRepository
{
    /**
     * @var MailmanApi
     */
    protected $api;
}
