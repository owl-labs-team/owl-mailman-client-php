<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Repository;

use OwlLabs\OwlMailman\Client\Data;
use OwlLabs\OwlMailman\Client\MailmanApi;
use OwlLabs\OwlMailman\Client\Repository\Response\Accepted;

/**
 * Class Template
 * @package OwlLabs\OwlMailman\Client\Repository
 */
class Template extends AbstractRepository
{
    /**
     * @var string
     */
    private $templateId;

    /**
     * Template constructor.
     * @param string $templateId
     * @param MailmanApi $api
     */
    public function __construct(string $templateId, MailmanApi $api)
    {
        $this->templateId = $templateId;
        $this->api = $api;
    }

    /**
     * @return string
     */
    public function templateId(): string
    {
        return $this->templateId;
    }

    /**
     * @param string $versionId
     * @return TemplateVersion
     */
    public function version(string $versionId): TemplateVersion
    {
        return new TemplateVersion($versionId, $this, $this->api);
    }

    /**
     * @return Data\Object\TemplateView
     */
    public function read(): Data\Object\TemplateView
    {
        $path = sprintf('/v%d/templates/%s', $this->api->version(), $this->templateId);
        $data = $this->api->get($path);
        return new Data\Object\TemplateView($data);
    }

    /**
     * @return Accepted
     */
    public function modify(Data\Input\TemplateModify $input): Accepted
    {
        $path = sprintf('/v%d/templates/%s', $this->api->version(), $this->templateId);
        return $this->api->patch($path, $input->toArray());
    }
}
