<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Repository\Response;

use Psr\Http\Message\ResponseInterface;

/**
 * Class Accepted
 * @package OwlLabs\OwlMailman\Client\Repository\Response
 */
class Accepted
{
    /**
     * @var string[][]
     */
    private $headers;

    /**
     * @param ResponseInterface $response
     * @return Accepted
     */
    public static function fromResponse(ResponseInterface $response): Accepted
    {
        $accepted = new self();
        $accepted->headers = $response->getHeaders();
        return $accepted;
    }

    /**
     * Accepted constructor.
     */
    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * @return string[][]
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasHeader(string $name): bool
    {
        return array_key_exists($name, $this->headers);
    }

    /**
     * @param string $name
     * @return array
     */
    public function getHeader(string $name): array
    {
        return $this->headers[$name];
    }

    /**
     * @param string $name
     * @param string $glue
     * @return string
     */
    public function getHeaderValue(string $name, string $glue = ', '): string
    {
        return implode($glue, $this->getHeader($name));
    }
}
