<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Repository\Response;

use Psr\Http\Message\ResponseInterface;
use Zend\Json\Json;

/**
 * Class BadRequest
 * @package OwlLabs\OwlMailman\Client\Repository\Response
 */
class BadRequest
{
    /**
     * @var string[][]
     */
    private $errors;

    /**
     * @param ResponseInterface $response
     * @return BadRequest
     */
    public static function fromResponse(ResponseInterface $response): BadRequest
    {
        $badRequest = new self();

        $data = Json::decode($response->getBody()->getContents(), Json::TYPE_ARRAY);

        $badRequest->errors = $data['errors'] ?? [];

        return $badRequest;
    }

    /**
     * BadRequest constructor.
     */
    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * @param string|null $name
     * @return string[][]|string[]
     */
    public function errors(string $name = null): array
    {
        if ($name === null) {
            return $this->errors;
        }
        if (array_key_exists($name, $this->errors)) {
            return $this->errors[$name];
        }
        return [];
    }
}
