<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Client\Repository;

use OwlLabs\OwlMailman\Client\Data\Object\RootView;
use OwlLabs\OwlMailman\Client\MailmanApi;

/**
 * Class Root
 * @package OwlLabs\OwlMailman\Client\Repository
 */
class Root
{
    /**
     * @var MailmanApi
     */
    private $api;

    /**
     * Root constructor.
     * @param MailmanApi $api
     */
    public function __construct(MailmanApi $api)
    {
        $this->api = $api;
    }

    /**
     * @return null|RootView
     */
    public function read(): ?RootView
    {
        $path = sprintf('/v%d', $this->api->version());
        $data = $this->api->get($path);
        return $data ? new RootView($data) : null;
    }
}
